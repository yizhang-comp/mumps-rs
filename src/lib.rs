//! Rust binding for MUMPS, serial version.

#![cfg_attr(feature = "lint", feature(plugin))]
#![cfg_attr(feature = "lint", plugin(clippy))]
#![cfg_attr(feature = "lint", allow(block_in_if_condition_stmt, needless_return))]
#![cfg_attr(not(test), allow(dead_code))]

extern crate libc;
extern crate MUMPS_sys as mumps;

type Mint = mumps::MUMPS_INT;
type Mreal = mumps::MUMPS_REAL;

pub struct Sol {
    pub par: mumps::DMUMPS_STRUC_C,
}

impl Sol {
    pub fn new(n: Mint, nz: Mint, par: Mint, sym: Mint) -> Sol {
        let mut sol = Sol { par: mumps::DMUMPS_STRUC_C::new(1, 1) };
        sol.par.par = par;
        sol.par.sym = sym;
        sol.par.comm_fortran = 0;
        sol.par.job = -1;
        unsafe {
            mumps::dmumps_c(&mut sol.par);
        }
        sol.par.n = n;
        sol.par.nz = nz;
        sol
    }

    pub fn sol(&mut self) {
        self.par.job = 6; // solve flag
        unsafe {
            mumps::dmumps_c(&mut self.par);
        }
    }

    pub fn get_rhs<'a>(&'a self) -> &'a [Mreal] {
        unsafe { std::slice::from_raw_parts(self.par.rhs, self.par.n as usize) }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mumps_1() {
        // same example from c_example.c
        let mut sol = Sol::new(2, 2, 1, 0);

        let mut irn: Vec<Mint> = vec![1, 2];
        sol.par.irn = &mut irn[0] as *mut Mint;

        let mut jcn: Vec<Mint> = vec![1, 2];
        sol.par.jcn = &mut jcn[0] as *mut Mint;

        let mut a: Vec<Mreal> = vec![1.0, 2.0];
        sol.par.a = &mut a[0] as *mut Mreal;

        let mut rhs: Vec<Mreal> = vec![1.0, 3.0];
        sol.par.rhs = &mut rhs[0] as *mut Mreal;

        // println!("{:?}", unsafe { *sol.par.irn.offset(1) });

        // control parameters
        sol.par.icntl[0] = -1;
        sol.par.icntl[1] = -1;
        sol.par.icntl[2] = -1;
        sol.par.icntl[3] = 0;

        sol.sol();

        // check solution

        let solution = sol.get_rhs();
        assert_eq!(1.0, solution[0]); // wrapper to get rhs
        assert_eq!(1.5, solution[1]);

        // assert_eq!(1.0, unsafe { *sol.par.rhs });
        // assert_eq!(1.5, unsafe { *sol.par.rhs.offset(1) });
    }
}
