# MUMPS-rs

Rust bindings and high-level wrappers for the direct linear
solver library MUMPS API.

Requires MUMPS library of version 5.1 or later. Currently
only serial version(without MPI support) is provided (due to
lack to robust Rust MPI bindings).

## Compatibility

### Platforms

`MUMPS-rs` is known to run on these platforms:

- Linux: x86_64 GNU/Linux (Fedora & Ubuntu)

### Rust

`MUMPS-rs` is tested for all three official release channels: stable, beta and nightly.

## Building

### pkg-config script
`MUMPS_sys` relies on `pkg-config` to find information of
`MUMPS` installation. The path to `libseq` of `MUMPS` should
be provided in `build.rs`.

### Linux

The build script of `MUMPS_sys` crate will try to use `pkg-config` if it's available
to deduce MUMPS library location. User must make sure
pkg-config is able to locate MUMPS static library.

Note that `cargo clean` is requred before rebuilding if any of those variables are changed.

### Build

First make sure all dependent libraries are being pointed
correctly in `mumps_sys/build.rs`, then `cargo build`. 

### Test

Use `cargo test` to run the test example from `MUMPS` manual
(a simple 2x2 matrix solution).

## License

`MUMPS-rs` is distributed under the terms of the
Apache License (Version 2.0). See [LICENSE-APACHE](LICENSE-APACHE)
for details.
