//! Rust binding for MUMPS.

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

extern crate libc;

// include bindings.rs from bindgen
#[link(name = "dmumps", kind = "static")]
#[link(name = "mumps_common", kind = "static")]
#[link(name = "pord", kind = "static")]
// #[link(name = "metis", kind = "static")]
// #[link(name = "mpi", kind = "dylib")]

#[link(name = "clang", kind = "dylib")]
#[link(name = "gfortran", kind = "dylib")]
#[link(name = "scalapack", kind = "static")]
#[link(name = "lapack", kind = "dylib")]
#[link(name = "blas", kind = "dylib")]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(not(feature = "int64"))]
pub type MUMPS_INT = ::std::os::raw::c_int;
#[cfg(feature = "int64")]
pub type MUMPS_INT = i64;

#[cfg(not(feature = "real32"))]
pub type MUMPS_REAL = f64;
#[cfg(feature = "real32")]
pub type MUMPS_REAL = f32;

#[cfg(not(feature = "real32"))]
pub type MUMPS_REAL2 = f64;
#[cfg(feature = "real32")]
pub type MUMPS_REAL2 = f32;

macro_rules! default_int_array_ptr {
    () => {
        &mut [0 as MUMPS_INT][0] as *mut MUMPS_INT
    }
}

macro_rules! default_real_array_ptr {
    () => {
        &mut [0.0 as MUMPS_REAL][0] as *mut MUMPS_REAL
    }
}

macro_rules! int_vec_ptr {
    ($n: expr) => {
        &mut vec![0 as MUMPS_INT; $n as usize][0] as *mut MUMPS_INT
    }
}

macro_rules! real_vec_ptr {
    ($n: expr) => {
        &mut vec![0.0 as MUMPS_REAL; $n as usize][0] as *mut MUMPS_REAL
    }
}

// RAII for DMUMPS_STRUCT_C
impl DMUMPS_STRUC_C {
    pub fn new(n: MUMPS_INT, nz: MUMPS_INT) -> DMUMPS_STRUC_C {
        assert!(n * n >= nz);
        DMUMPS_STRUC_C {
            sym: 0,
            par: 0,
            job: 0,
            comm_fortran: 0,
            icntl: [0; 40usize],
            keep: [0; 500usize],
            cntl: [0.0; 15usize],
            dkeep: [0.0; 130usize],
            keep8: [0; 150usize],
            n: n,
            nz_alloc: 0,
            nz: nz,
            irn: int_vec_ptr!(nz),
            jcn: int_vec_ptr!(nz),
            a: real_vec_ptr!(nz),
            nz_loc: 0,
            irn_loc: default_int_array_ptr!(),
            jcn_loc: default_int_array_ptr!(),
            a_loc: default_real_array_ptr!(),
            nelt: 0,
            eltptr: default_int_array_ptr!(),
            eltvar: default_int_array_ptr!(),
            a_elt: default_real_array_ptr!(),
            perm_in: default_int_array_ptr!(),
            sym_perm: default_int_array_ptr!(),
            uns_perm: default_int_array_ptr!(),
            colsca: default_real_array_ptr!(),
            rowsca: default_real_array_ptr!(),
            colsca_from_mumps: 0,
            rowsca_from_mumps: 0,
            rhs: real_vec_ptr!(n),
            redrhs: default_real_array_ptr!(),
            rhs_sparse: default_real_array_ptr!(),
            sol_loc: default_real_array_ptr!(),
            irhs_sparse: default_int_array_ptr!(),
            irhs_ptr: default_int_array_ptr!(),
            isol_loc: default_int_array_ptr!(),
            nrhs: 0,
            lrhs: 0,
            lredrhs: 0,
            nz_rhs: 0,
            lsol_loc: 0,
            schur_mloc: 0,
            schur_nloc: 0,
            schur_lld: 0,
            mblock: 0,
            nblock: 0,
            nprow: 0,
            npcol: 0,
            info: [0; 40usize],
            infog: [0; 40usize],
            rinfo: [0.0; 40usize],
            rinfog: [0.0; 40usize],
            deficiency: 0,
            pivnul_list: default_int_array_ptr!(),
            mapping: default_int_array_ptr!(),
            size_schur: 0,
            listvar_schur: default_int_array_ptr!(),
            schur: default_real_array_ptr!(),
            instance_number: 0,
            wk_user: default_real_array_ptr!(),
            version_number: [0; 27usize],
            ooc_tmpdir: [0; 256usize],
            ooc_prefix: [0; 64usize],
            write_problem: [0; 256usize],
            lwk_user: 0,
        }
    }
}

#[allow(unused_mut)]
#[cfg(test)]
mod tests {
    use super::*;

    // the same example problem as in MUMPS manual for C
    // interface
    #[test]
    fn mumps_c() {
        const JOB_INIT: MUMPS_INT = -1;
        const JOB_END: MUMPS_INT = -2;

        let n: MUMPS_INT = 2;
        let nz: MUMPS_INT = 2;
        // initial dmumps size doesn't care, real init
        // happens with dmumps_c call with JOB_INIT
        let mut dmumps_par = DMUMPS_STRUC_C::new(1, 1);

        dmumps_par.par = 1;
        dmumps_par.sym = 0;
        dmumps_par.comm_fortran = 0;
        dmumps_par.job = JOB_INIT;
        // dmumps_par.n = 0;
        // dmumps_par.nz = 0;
        unsafe {
            // create MUMPS instance
            dmumps_c(&mut dmumps_par);
        }
        dmumps_par.n = n;
        dmumps_par.nz = nz;

        let mut irn: Vec<MUMPS_INT> = vec![1, 2];
        dmumps_par.irn = &mut irn[0] as *mut MUMPS_INT;

        let mut jcn: Vec<MUMPS_INT> = vec![1, 2];
        dmumps_par.jcn = &mut jcn[0] as *mut MUMPS_INT;

        let mut a: Vec<MUMPS_REAL> = vec![1.0, 2.0];
        dmumps_par.a = &mut a[0] as *mut MUMPS_REAL;

        let mut rhs: Vec<MUMPS_REAL> = vec![1.0, 3.0];
        dmumps_par.rhs = &mut rhs[0] as *mut MUMPS_REAL;

        assert_eq!(1, unsafe { *dmumps_par.irn });
        assert_eq!(2, unsafe { *dmumps_par.irn.offset(1) });
        assert_eq!(1, unsafe { *dmumps_par.jcn });
        assert_eq!(2, unsafe { *dmumps_par.jcn.offset(1) });
        assert_eq!(1.0, unsafe { *dmumps_par.a });
        assert_eq!(2.0, unsafe { *dmumps_par.a.offset(1) });
        assert_eq!(1.0, unsafe { *dmumps_par.rhs });
        assert_eq!(3.0, unsafe { *dmumps_par.rhs.offset(1) });

        // control parameters
        dmumps_par.icntl[0] = -1;
        dmumps_par.icntl[1] = -1;
        dmumps_par.icntl[2] = -1;
        dmumps_par.icntl[3] = 0;

        dmumps_par.job = 6;
        unsafe {                // solve MUMPS instance
            dmumps_c(&mut dmumps_par);
        }

        // check solution
        // println!("{:?}", unsafe { *dmumps_par.rhs });
        // println!("{:?}", unsafe { *dmumps_par.rhs.offset(1) });
        assert_eq!(1.0, unsafe { *dmumps_par.rhs });
        assert_eq!(1.5, unsafe { *dmumps_par.rhs.offset(1) });
    }
}
