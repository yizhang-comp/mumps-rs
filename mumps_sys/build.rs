extern crate pkg_config;
extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system 
    // shared libraries.
    println!(r"cargo:rustc-link-search=/usr/local/lib64");
    println!(r"cargo:rustc-link-search=/usr/lib64/llvm");
    println!(r"cargo:rustc-link-search=/usr/local/lib");
    println!(r"cargo:rustc-link-search=/usr/lib/llvm-3.8/lib");
    println!(r"cargo:rustc-link-search=/usr/local/lib");
    println!(r"cargo:rustc-link-lib=dylib=clang");

    // Tell cargo to tell rustc to link the double precision MUMPS
    // lib through pkg-config
    pkg_config::Config::new().statik(true).probe("dmumps").unwrap();
    println!(r"cargo:rustc-link-lib=dylib=gfortran");
    println!(r"cargo:rustc-link-lib=dylib=m");
    println!(r"cargo:rustc-link-lib=dylib=pthread");
    println!(r"cargo:rustc-link-lib=blas");
    println!(r"cargo:rustc-link-lib=lapack");

    // serial MUMPS has "libseq" as mock MPI libs. Pointing
    // the path to libseq in the following path
    println!(r"cargo:rustc-link-search=/home/MUMPS_5.0.2/libseq");
    println!(r"cargo:rustc-link-lib=static=mpiseq");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        .generate_comments(false)
        .header("wrapper.h")
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
